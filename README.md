API for client project

Required environmental variables with explainations:


# Postgres ---------------
POSTGRES_VERSION-- which docker postgres image version you would like to use

POSTGRES_PATH-- path to save docker postgres data

POSTGRES_PORT-- postgres db port to listen on

POSTGRES_PASSWORD-- postgres db password

POSTGRES_USER-- postgres db username

POSTGRES_DB-- postgres db name

POSTGRES_TEST_PORT-- postgres test db port to listen on

# TypeOrm ----------------
TYPEORM_CONNECTION-- type of connection to use for typeorm, here we are using 'postgres'

TYPEORM_HOST-- location of host, unless remote, use 'localhost'

TYPEORM_PORT-- port where db is listening for connections

TYPEORM_USERNAME-- db username

TYPEORM_PASSWORD-- db password

TYPEORM_DATABASE-- db name

TYPEORM_MIGRATIONS_TABLE_NAME-- name of table to use for storing migration history

TYPEORM_MIGRATIONS_DIR-- location of your migrations directory so typeorm can generate them in the right place, use 'src/migrations'

TYPEORM_ENTITIES-- location of your entities so typeorm can generate migrations for them, use 'src/model/entity/*.ts'

TYPEORM_MIGRATIONS-- location of your migrations so typeorm can use them to determine migration history and generate new ones, use 'src/migrations/*.ts'

# Auth ----------------
JWT_SECRET-- secret key used in jwt signing, use any random string here, just dont share it!

# General -----------------
API_VERSION-- api version used as route prefix, eg. 'v1' or 'api'

PORT-- port where app is listening

# How to start --------------

Make sure to have docker and docker-compose installed, along with your environmental variables set.
After cloning the project:
```bash
cd workout-api
npm run start
docker-compose pull
docker-compose up -d
```