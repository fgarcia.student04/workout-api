import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TestDatabaseModule } from '../src/modules/test/database.test.module';
import { ControllerModule } from '../src/modules/controller.module';
import { runMigrations } from '../src/utilities/runMigrations.utilities';
import { CustomWsAdapter } from '../src/socket/wsadapter.socket';
export function post(app: INestApplication, url: string, body: any, headers?: Array<[string, string]>) {
  const httpRequest = request(app.getHttpServer()).post(url);
  setBody(httpRequest, body);
  httpRequest.set('Accept', 'application/json');
  setHeaders(httpRequest, headers);
  return httpRequest;
}

export function get(app: INestApplication, url: string, headers?: Array<[string, string]>) {
  const httpRequest = request(app.getHttpServer()).get(url);
  setHeaders(httpRequest, headers);
  return httpRequest;
}

export function put(app: INestApplication, url: string, body: any, headers?: Array<[string, string]>) {
  const httpRequest = request(app.getHttpServer()).put(url);
  setBody(httpRequest, body);
  httpRequest.set('Accept', 'application/json');
  setHeaders(httpRequest, headers);
  return httpRequest;
}

export function del(app: INestApplication, url: string, headers?: Array<[string, string]>) {
  const httpRequest = request(app.getHttpServer()).del(url);
  setHeaders(httpRequest, headers);
  return httpRequest;
}

function setBody(req: request.Test, body: any) {
  req.send(body);
}

function setHeaders(req: request.Test, headers?: Array<[string, string]>) {
  if (headers) {
    headers.forEach(([field, value]) => req.set(field, value));
  }
}

export async function createTestingApplication() {
  const moduleFixture = await Test.createTestingModule({
    imports: [
      TestDatabaseModule,
      ControllerModule,
    ],
  })
  .compile();
  const app = moduleFixture.createNestApplication();
  app.useWebSocketAdapter(new CustomWsAdapter(app));
  await app.init();
  await runMigrations(app);
  return app;
}
