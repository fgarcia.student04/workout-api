import { INestApplication, HttpStatus } from '@nestjs/common';
import { UserTypeEnum } from '../src/model/enum/userType.enum';
import { get, post, del, createTestingApplication } from './tools';
import { UserVm } from '../src/model/vm/user.vm';
import { AdminUserSubmissionVm } from '../src/model/vm/adminUserSubmission.vm';
import { UserManager } from '../src/manager/Implementation/user.manager';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let createdUser: UserVm;

  beforeEach(async () => {
    app = await createTestingApplication();
    const adminUser = new AdminUserSubmissionVm();
    adminUser.firstName = 'Test';
    adminUser.lastName = 'User';
    adminUser.email = 'rootAdmin@user.com';
    adminUser.password = 'secret';
    adminUser.userType = UserTypeEnum.Client;
    adminUser.roles = [1, 2];
    createdUser = await app.get(UserManager).createUserAdmin(adminUser, 1);
    const loginRes = await post(app, '/auth/login', { email: adminUser.email, password: adminUser.password });
    accessToken = loginRes.body.access_token;
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
  });

  it('/users (GET)', async () => {
    const res = await get(app, '/users', [['Authorization', `Bearer ${accessToken}`]]);
    expect(res.status).toEqual(HttpStatus.OK);
    expect(Array.isArray(res.body)).toBeTruthy();
  });

  it('/users/:id (GET)', async () => {
    const res = await get(app, `/users/${createdUser.id}`, [['Authorization', `Bearer ${accessToken}`]]);
    expect(res.status).toEqual(HttpStatus.OK);
    const user: UserVm = res.body;
    expect(user).not.toBe(undefined);
    expect(createdUser.id).toEqual(user.id);
    expect(createdUser.email).toEqual(user.email);
    expect(createdUser.name).toEqual(user.name);
    expect(createdUser.userType).toEqual(user.userType);
    expect(createdUser.userTypeDescription).toEqual(user.userTypeDescription);
    expect(createdUser.roles.length).toEqual(user.roles.length);
  });

  it('/users/:id (DELETE)', async () => {
    const res = await del(app, `/users/${createdUser.id}`, [['Authorization', `Bearer ${accessToken}`]]);
    expect(res.status).toEqual(HttpStatus.NO_CONTENT);
  });
});
