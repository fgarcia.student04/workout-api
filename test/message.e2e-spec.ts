import { INestApplication, HttpStatus } from '@nestjs/common';
import { UserTypeEnum } from '../src/model/enum/userType.enum';
import { UserManager } from '../src/manager/Implementation/user.manager';
import { post, createTestingApplication, get } from './tools';
import { UserSubmissionVm } from '../src/model/vm/userSubmission.vm';
import { UserVm } from '../src/model/vm/user.vm';
import { MessageManager } from '../src/manager/Implementation/message.manager';
import { MessageSubmissionVm } from '../src/model/vm/messageSubmission.vm';
describe('MessageController (e2e)', () => {
  let app: INestApplication;
  let createdUserAccessToken: string;
  let createdUser: UserVm;
  let createdUser2: UserVm;

  function buildTestUserSubmissionVm() {
    return {
      firstName: 'Test',
      lastName: 'User',
      email: 'test2@user.com',
      password: 'secret',
      userType: UserTypeEnum.Client,
    } as UserSubmissionVm;
  }

  beforeEach(async () => {
    app = await createTestingApplication();
    const user = new UserSubmissionVm();
    user.firstName = 'Test';
    user.lastName = 'User';
    user.email = 'test@user.com';
    user.password = 'secret';
    user.userType = UserTypeEnum.Client;
    createdUser = await app.get(UserManager).createUser(user);
    user.email = 'test2@user.com';
    createdUser2 = await app.get(UserManager).createUser(user);
    const loginRes = await post(app, '/auth/login', { email: 'test@user.com', password: user.password });
    createdUserAccessToken = loginRes.body.access_token;

    const messageManager = app.get(MessageManager);
    const newMessage = new MessageSubmissionVm();
    newMessage.content = 'Hey this is a test message';
    newMessage.senderId = createdUser.id;
    newMessage.recipientId = createdUser2.id;
    await messageManager.createMessage(newMessage, createdUser.id);

    const newMessage2 = new MessageSubmissionVm();
    newMessage2.content = 'Hey this is a test response';
    newMessage2.senderId = createdUser2.id;
    newMessage2.recipientId = createdUser.id;
    const message2 = await messageManager.createMessage(newMessage2, createdUser2.id);
    await messageManager.updateMessagesToUnread([message2.id], createdUser2.id);
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
  });

  describe('/messages (GET)', () => {
    it('returns 1 unread message', async () => {
      const res = await get(app, '/messages', [['Authorization', `Bearer ${createdUserAccessToken}`]]);
      expect(res.status).toEqual(HttpStatus.OK);
      expect(Array.isArray(res.body)).toBeTruthy();
      expect(res.body.length).toEqual(1);
    });
  });

  describe('/messages/conversation/:id (GET)', () => {
    it('returns 2 messages', async () => {
      const res = await get(app, `/messages/conversation/${createdUser2.id}`, [['Authorization', `Bearer ${createdUserAccessToken}`]]);
      expect(res.status).toEqual(HttpStatus.OK);
      expect(Array.isArray(res.body)).toBeTruthy();
      expect(res.body.length).toEqual(2);
    });
  });
});
