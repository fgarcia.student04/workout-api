import * as dotenv from 'dotenv';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { UserTypeEnum } from '../src/model/enum/userType.enum';
import { UserSubmissionVm } from '../src/model/vm/userSubmission.vm';
import { UserVm } from '../src/model/vm/user.vm';
import { post, get, createTestingApplication } from './tools';

dotenv.config();

describe('Authentication Controller (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    app = await createTestingApplication();
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
  });

  function createValidSampleUser() {
    return {
      firstName: 'Test',
      lastName: 'User',
      email: 'test@user.com',
      password: 'secret',
      userType: UserTypeEnum.Client,
    } as UserSubmissionVm;
  }

  it('/register (POST)', async () => {
    const user = createValidSampleUser();
    const res = await post(app, '/auth/register', user);
    expect(res.status).toEqual(HttpStatus.CREATED);
    const createdUser: UserVm = res.body;
    expect(createdUser.id).not.toBe(0);
    expect(createdUser.email).toEqual(user.email);
    expect(createdUser.name).toEqual(`${user.firstName} ${user.lastName}`);
    expect(createdUser.userType).toEqual(user.userType);
    expect(createdUser.userTypeDescription).toEqual('Client');
    expect(createdUser.roles.length).toEqual(1);
  });

  it('/login (POST)', async () => {
    // create user
    const user = createValidSampleUser();
    const res = await post(app, '/auth/register', user);
    const createdUser: UserVm = res.body;

    // login with the user to recieve a jwt
    const loginRes = await post(app, '/auth/login', { email: createdUser.email, password: user.password });
    expect(loginRes.status).toEqual(HttpStatus.CREATED);
    const accessToken = loginRes.body.access_token;
    expect(accessToken).not.toBe(undefined);
  });

  it('/me (GET)', async () => {
    // create user
    const user = createValidSampleUser();
    const res = await post(app, '/auth/register', user);
    const createdUser: UserVm = res.body;

    // login with the user to recieve a jwt
    const loginRes = await post(app, '/auth/login', { email: createdUser.email, password: user.password });
    const accessToken = loginRes.body.access_token;

    const headers: Array<[string, string]> = [
      ['Authorization', `Bearer ${accessToken}`],
    ];
    const meRes = await get(app, '/auth/me', headers);
    expect(meRes.status).toEqual(HttpStatus.OK);
    const meUser: UserVm = meRes.body;
    expect(meUser.id).toEqual(createdUser.id);
    expect(meUser.email).toEqual(createdUser.email);
    expect(meUser.name).toEqual(createdUser.name);
    expect(meUser.userType).toEqual(createdUser.userType);
    expect(meUser.userTypeDescription).toEqual(createdUser.userTypeDescription);
    expect(meUser.roles.length).toEqual(1);
  });
});
