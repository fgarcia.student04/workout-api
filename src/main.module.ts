import { Module } from '@nestjs/common';
import { ControllerModule } from './modules/controller.module';
import { DatabaseModule } from './modules/database.module';

@Module({
  imports: [
    DatabaseModule,
    ControllerModule,
  ],
})
export class MainModule {}
