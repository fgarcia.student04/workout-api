import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import 'reflect-metadata';
import * as dotenv from 'dotenv';
import { runMigrations } from './utilities/runMigrations.utilities';
import { CustomWsAdapter } from './socket/wsadapter.socket';

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(MainModule);

  app.setGlobalPrefix(process.env.API_VERSION);
  app.useWebSocketAdapter(new CustomWsAdapter(app));
  await app.listen(process.env.PORT);
  await runMigrations(app);
}
bootstrap();
