import { MessageEntity } from '../../model/entity/message.entity';

export interface IMessageRepository {
  getAllUnreadMessagesWithRecipientId: (recipientId: number) => Promise<MessageEntity[]>;
  getConservasionBetweenUsers: (userIdA: number, userIdB: number) => Promise<MessageEntity[]>;
  createMessage: (message: MessageEntity) => Promise<MessageEntity>;
  updateMessagesToUnread: (messageIds: number[], modifierId: number) => Promise<MessageEntity[]>;
  updateMessagesToRead: (messageIds: number[], modifierId: number) => Promise<MessageEntity[]>;
}
