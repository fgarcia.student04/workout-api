import { UserEntity } from '../../model/entity/user.entity';

export interface IUserRepository {
  getAllUsers: () => Promise<UserEntity[]>;
  createUser: (user: UserEntity, roles: number[]) => Promise<UserEntity>;
  getUserById: (userId: number) => Promise<UserEntity>;
  getUserByEmail: (email: string) => Promise<UserEntity | null>;
  deleteUserById: (userId: number) => Promise<UserEntity>;
}
