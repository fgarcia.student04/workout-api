import { IUserRepository } from '../Interface/iuser.repository';
import { Connection, Repository } from 'typeorm';
import { UserEntity } from '../../model/entity/user.entity';
import { Injectable } from '@nestjs/common';
import { EntityNotFoundException } from '../../utilities/exception/entityNotFound.exception';
import { RoleEntity } from '../../model/entity/role.entity';

@Injectable()
export class UserRepository implements IUserRepository {

  private readonly roles: Repository<RoleEntity>;
  private readonly users: Repository<UserEntity>;
  constructor(
    private readonly connection: Connection,
  ) {
    this.users = connection.getRepository(UserEntity);
    this.roles = connection.getRepository(RoleEntity);
  }

  public getAllUsers = async () => {
    return await this.users.find({ relations: ['roles'] });
  }

  public createUser = async (user: UserEntity, roles: number[]) => {
    if (roles.length > 0) {
      const rolesToAdd = await this.roles.findByIds(roles);
      user.roles = rolesToAdd;
    }
    return await this.users.save(user);
  }

  public getUserById = async (userId: number) => {
    const user = await this.users.findOne(userId, { relations: ['roles'] });
    if (!user) {
      throw new EntityNotFoundException(userId, 'User');
    }
    return user;
  }

  public getUserByEmail = async (email: string) => {
    const user = await this.users.findOne({ where: { email }});
    return user;
  }

  public deleteUserById = async (userId: number) => {
    const user = await this.users.findOne(userId);
    if (!user) {
      throw new EntityNotFoundException(userId, 'User');
    }
    user.isActive = false;
    user.isDeleted = true;
    return await this.users.save(user);
  }
}
