import { IMessageRepository } from '../Interface/imessage.repository';
import { MessageEntity } from '../../model/entity/message.entity';
import { Repository, Connection } from 'typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MessageRepository implements IMessageRepository {
  private readonly messages: Repository<MessageEntity>;
  constructor(
    private readonly connection: Connection,
  ) {
    this.messages = connection.getRepository(MessageEntity);
  }

  public getAllUnreadMessagesWithRecipientId = async (recipientId: number) => {
    const messages = await this.messages.find({ where: { recipientId, unread: true } });
    return messages;
  }

  public getConservasionBetweenUsers = async (userIdA: number, userIdB: number) => {
    const messages = await this.messages.find({
      where: [
        { senderId: userIdA, recipientId: userIdB },
        { senderId: userIdB, recipientId: userIdA },
      ],
      order: { dateCreated: 'ASC' },
    });
    return messages;
  }

  public createMessage = async (message: MessageEntity) => {
    return await this.messages.save(message);
  }

  public updateMessagesToUnread = async (messageIds: number[], modifierId: number) => {
    const messages = await this.messages.findByIds(messageIds);
    messages.forEach((m) => {
      m.unread = true;
    });
    return await this.messages.save(messages);
  }

  public updateMessagesToRead = async (messageIds: number[], modifierId: number) => {
    const messages = await this.messages.findByIds(messageIds);
    messages.forEach((m) => {
      m.unread = false;
    });
    return await this.messages.save(messages);
  }
}
