import { UserRepository } from './Implementation/user.repository';
import { MessageRepository } from './Implementation/message.repository';

export const repositories = [
  UserRepository,
  MessageRepository,
];
