import { UserManager } from './Implementation/user.manager';
import { AuthenticationManager } from './Implementation/authentication.manager';
import { MessageManager } from './Implementation/message.manager';

export const managers = [
  UserManager,
  AuthenticationManager,
  MessageManager,
];
