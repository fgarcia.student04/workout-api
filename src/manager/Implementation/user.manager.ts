import { IUserManager } from '../Interface/iuser.manager';
import { Mapper } from '../../model/mapping/mapper';
import { UserVm } from '../../model/vm/user.vm';
import { Injectable } from '@nestjs/common';
import { UserRepository } from '../../repository/Implementation/user.repository';
import { UserEntity } from '../../model/entity/user.entity';
import { UserSubmissionVm } from '../../model/vm/userSubmission.vm';
import { UserDto } from '../../model/dto/user.dto';
import { RoleVm } from '../../model/vm/role.vm';
import { RoleEnum } from '../../model/enum/role.enum';
import { AdminUserSubmissionVm } from '../../model/vm/adminUserSubmission.vm';

@Injectable()
export class UserManager implements IUserManager {

  constructor(
    private readonly userRepository: UserRepository,
    private readonly mapper: Mapper,
  ) {}

  public getAllUsers = async () => {
    const users = await this.userRepository.getAllUsers();
    const userRolesMap = {};
    users.forEach((userEntity) => {
      userRolesMap[userEntity.id] = this.mapper.Map(userEntity.roles, RoleVm);
    });
    const userViews = this.mapper.Map(users, UserVm);
    userViews.forEach((userView) => {
      userView.roles = userRolesMap[userView.id];
    });
    return userViews;
  }

  public createUser = async (user: UserSubmissionVm) => {
    const entityToCreate = this.mapper.Map(user, UserEntity);
    const newlyCreatedEntity = await this.userRepository.createUser(entityToCreate, [RoleEnum.User]);
    const userView = this.mapper.Map(newlyCreatedEntity, UserVm);
    userView.roles = this.mapper.Map(newlyCreatedEntity.roles, RoleVm);
    return userView;
  }

  public createUserAdmin = async (user: AdminUserSubmissionVm, authorId: number) => {
    const entityToCreate = this.mapper.Map(user, UserEntity);
    entityToCreate.authorId = authorId;
    const newlyCreatedEntity = await this.userRepository.createUser(entityToCreate, user.roles);
    const userView = this.mapper.Map(newlyCreatedEntity, UserVm);
    userView.roles = this.mapper.Map(newlyCreatedEntity.roles, RoleVm);
    return userView;
  }

  public getUserById = async (userId: number) => {
    const user = await this.userRepository.getUserById(userId);
    const userView = this.mapper.Map(user, UserVm);
    userView.roles = this.mapper.Map(user.roles, RoleVm);
    return userView;
  }

  public getUserByEmail = async (email: string) => {
    const user = await this.userRepository.getUserByEmail(email);
    if (user) {
      return this.mapper.Map(user, UserDto);
    }
    return null;
  }

  public deleteUserById = async (userId: number) => {
    await this.userRepository.deleteUserById(userId);
    return true;
  }
}
