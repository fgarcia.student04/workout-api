import { IMessageManager } from '../Interface/imessage.manager';
import { MessageSubmissionVm } from '../../model/vm/messageSubmission.vm';
import { MessageEntity } from '../../model/entity/message.entity';
import { MessageRepository } from '../../repository/Implementation/message.repository';
import { Mapper } from '../../model/mapping/mapper';
import { MessageVm } from '../../model/vm/message.vm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MessageManager implements IMessageManager {

  constructor(
    private readonly messageRepository: MessageRepository,
    private readonly mapper: Mapper,
  ) {}

  public getAllUnreadMessagesWithRecipientId = async (recipientId: number) => {
    const messageEntities = await this.messageRepository.getAllUnreadMessagesWithRecipientId(recipientId);
    const messageViews = this.mapper.Map(messageEntities, MessageVm);
    return messageViews;
  }

  public getConservasionBetweenUsers = async (userIdA: number, userIdB: number) => {
    const messageEntities = await this.messageRepository.getConservasionBetweenUsers(userIdA, userIdB);
    const messageViews = this.mapper.Map(messageEntities, MessageVm);
    return messageViews;
  }

  public createMessage = async (message: MessageSubmissionVm, authorId: number) => {
    let messageEntity = this.mapper.Map(message, MessageEntity);
    messageEntity.authorId = authorId;
    messageEntity.unread = false;
    messageEntity = await this.messageRepository.createMessage(messageEntity);
    const messageView = this.mapper.Map(messageEntity, MessageVm);
    return messageView;
  }

  public updateMessagesToUnread = async (messageIds: number[], modifierId: number) => {
    const messageEntity = await this.messageRepository.updateMessagesToUnread(messageIds, modifierId);
    const messageView = this.mapper.Map(messageEntity, MessageVm);
    return messageView;
  }
}
