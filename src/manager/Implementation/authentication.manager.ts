import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenPayload } from '../../model/auth/tokenPayload.auth';
import { UserManager } from './user.manager';
import * as bcrypt from 'bcrypt';
import { IAuthenticationManager } from '../Interface/iauthentication.manager';

@Injectable()
export class AuthenticationManager implements IAuthenticationManager {

  constructor(
    private readonly jwtService: JwtService,
    private readonly userManager: UserManager,
  ) {}

  public verifyUser = async (email: string, password: string): Promise<TokenPayload> => {
    const user = await this.userManager.getUserByEmail(email);
    if (user && bcrypt.compareSync(password, user.password)) {
      return {
        email,
        userId: user.id,
      };
    }
    return null;
  }

  public login = async (user: TokenPayload) => {
    // create jwt token here and return to client for
    // authenticating future calls to the api
    const payload = { email: user.email, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  public decode = (token: string) => {
    return this.jwtService.decode(token);
  }
}
