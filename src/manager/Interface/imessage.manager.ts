import { MessageSubmissionVm } from '../../model/vm/messageSubmission.vm';
import { MessageVm } from '../../model/vm/message.vm';
export interface IMessageManager {
  getAllUnreadMessagesWithRecipientId: (recipientId: number) => Promise<MessageVm[]>;
  getConservasionBetweenUsers: (userIdA: number, userIdB: number) => Promise<MessageVm[]>;
  createMessage: (message: MessageSubmissionVm, authorId: number) => Promise<MessageVm>;
  updateMessagesToUnread: (messageIds: number[], modifierId: number) => Promise<MessageVm[]>;
}
