import { UserVm } from '../../model/vm/user.vm';
import { UserSubmissionVm } from '../../model/vm/userSubmission.vm';
import { UserDto } from '../../model/dto/user.dto';

export interface IUserManager {
  getAllUsers: () => Promise<UserVm[]>;
  createUser: (user: UserSubmissionVm) => Promise<UserVm>;
  createUserAdmin: (user: UserSubmissionVm, authorId: number) => Promise<UserVm>;
  getUserById: (userId: number) => Promise<UserVm>;
  getUserByEmail: (email: string) => Promise<UserDto | null>;
  deleteUserById: (userId: number) => Promise<boolean>;
}
