export interface IAuthenticationManager {
  verifyUser: (email: string, password: string) => Promise<any>;
  login: (user: any) => Promise<any>;
  decode: (token: string) => any;
}
