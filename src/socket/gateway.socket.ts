import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  MessageBody,
  OnGatewayConnection,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Server as WSServer } from 'ws';
import { UserManager } from '../manager/Implementation/user.manager';
import * as querystring from 'querystring';
import { UserVm } from '../model/vm/user.vm';
import WebSocket = require('ws');
import { AuthenticationManager } from '../manager/Implementation/authentication.manager';
import { ValidationPipe } from '@nestjs/common';
import { MessageSubmissionVm } from '../model/vm/messageSubmission.vm';
import { Mapper } from '../model/mapping/mapper';
import { MessageVm } from '../model/vm/message.vm';
import { MessageUserVm } from '../model/vm/messageUser.vm';
import { MessageManager } from '../manager/Implementation/message.manager';
import { EventTypes } from '../utilities/constant.utilities';

type WebSocketWithUser = WebSocket & { user: UserVm };

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection {
  constructor(
    private readonly userManager: UserManager,
    private readonly authManager: AuthenticationManager,
    private readonly messageManager: MessageManager,
    private readonly mapper: Mapper,
  ) {}

  @WebSocketServer()
  private readonly wsserver?: WSServer;

  private sendMessage = (
    client: WebSocketWithUser,
    event: string,
    data: any,
    cb?: (err: Error) => void,
  ) => {
    client.send(JSON.stringify(this.buildPayload(event, data)), cb);
  }

  private buildPayload = (event: string, data: any) => ({ event, data });

  public async handleConnection(client: WebSocketWithUser, req: Request) {
    const query = querystring.parse(req.url.substring(req.url.indexOf('?') + 1));
    const token = query.token as string;
    const jwtPayload = this.authManager.decode(token);
    const userVm = await this.userManager.getUserById(jwtPayload.sub);
    client.user = userVm;
  }

  @SubscribeMessage('messageUser')
  public async messageUser(
    @ConnectedSocket() client: WebSocketWithUser,
    @MessageBody(new ValidationPipe({ transform: true })) data: MessageSubmissionVm,
  ) {
    try {
      const savedMessage = await this.messageManager.createMessage(data, client.user.id);
      if (this.wsserver && client.user) {
        let userFound = false;
        this.wsserver.clients.forEach((ws: WebSocketWithUser) => {
          if (ws.user.id === data.recipientId) {
            userFound = true;
            const messageToSend = this.mapper.Map(data, MessageVm);
            messageToSend.recipient = this.mapper.Map(ws.user, MessageUserVm);
            messageToSend.sender = this.mapper.Map(client.user, MessageUserVm);
            this.sendMessage(ws, EventTypes.Message, messageToSend, async (err) => {
              if (err) {
                this.sendMessage(client, EventTypes.Error, {message: 'Message failed to send, but was stored to be sent later.', error: err});
                await this.messageManager.updateMessagesToUnread([savedMessage.id], client.user.id);
              }
            });
          }
        });
        if (!userFound) {
          this.sendMessage(client, EventTypes.Info, {message: 'User does not appear to be online. They will recieve the message once they log in.'});
          await this.messageManager.updateMessagesToUnread([savedMessage.id], client.user.id);
        }
      }
    } catch (e) {
      this.sendMessage(client, EventTypes.Error, {message: 'There was an error processing your message', error: e});
    }
  }
}
