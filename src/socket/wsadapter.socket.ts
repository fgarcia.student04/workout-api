import { WsAdapter } from '@nestjs/platform-ws';
import * as ws from 'ws';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as querystring from 'querystring';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from '../repository/Implementation/user.repository';

export class CustomWsAdapter extends WsAdapter {
  constructor(
    private readonly app: INestApplication,
  ) {
    super(app);
  }

  public create(port, options) {
    const { server, ...wsOptions } = options;
    if (port === 0 && this.httpServer) {
        const wsServer = new ws.Server({
          ...wsOptions,
          server: this.httpServer,
          verifyClient: async (info, cb) => {
            try {
              const query = querystring.parse(info.req.url.substring(info.req.url.indexOf('?') + 1));
              const token = query.token as string;
              if (!token) {
                return cb(false, HttpStatus.UNAUTHORIZED, null, { Reason: 'Unauthorized' });
              }
              // decode token
              const jwtToken = this.app.get(JwtService).verify(token);
              if (!jwtToken) {
                return cb(false, HttpStatus.UNAUTHORIZED, null, { Reason: 'Unauthorized' });
              }
              // verify that user is valid
              const user = await this.app.get(UserRepository).getUserById(jwtToken.sub);
              if (!user.isActive || user.isDeleted) {
                return cb(false, HttpStatus.UNAUTHORIZED, null, { Reason: 'User is inactive or deleted' });
              }
              return cb(true);
            } catch (e) {
              return cb(false, HttpStatus.INTERNAL_SERVER_ERROR, null, { Reason: e });
            }
          },
        });
        return this.bindErrorHandler(wsServer);
    }
    return server;
  }
}
