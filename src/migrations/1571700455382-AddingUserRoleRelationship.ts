import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddingUserRoleRelationship1571700455382 implements MigrationInterface {
    name = 'AddingUserRoleRelationship1571700455382';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "user_roles__role" ("userId" integer NOT NULL, "roleId" integer NOT NULL, CONSTRAINT "PK_d7f198bc94dc001855679243777" PRIMARY KEY ("userId", "roleId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_0ac1a679cb8036902d99fd7324" ON "user_roles__role" ("userId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_0c863994b238c0c73aae6e18f7" ON "user_roles__role" ("roleId") `, undefined);
        await queryRunner.query(`ALTER TABLE "user_roles__role" ADD CONSTRAINT "FK_0ac1a679cb8036902d99fd7324a" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "user_roles__role" ADD CONSTRAINT "FK_0c863994b238c0c73aae6e18f7a" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user_roles__role" DROP CONSTRAINT "FK_0c863994b238c0c73aae6e18f7a"`, undefined);
        await queryRunner.query(`ALTER TABLE "user_roles__role" DROP CONSTRAINT "FK_0ac1a679cb8036902d99fd7324a"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_0c863994b238c0c73aae6e18f7"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_0ac1a679cb8036902d99fd7324"`, undefined);
        await queryRunner.query(`DROP TABLE "user_roles__role"`, undefined);
    }

}
