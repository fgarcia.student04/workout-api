import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddingNameToRole1571490453646 implements MigrationInterface {
    name = 'AddingNameToRole1571490453646';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Role" ADD "name" character varying NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Role" DROP COLUMN "name"`, undefined);
    }

}
