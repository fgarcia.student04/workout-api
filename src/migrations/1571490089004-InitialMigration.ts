import {MigrationInterface, QueryRunner} from 'typeorm';

export class InitialMigration1571490089004 implements MigrationInterface {
    name = 'InitialMigration1571490089004';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "Role" ("id" SERIAL NOT NULL, "dateCreated" TIMESTAMP NOT NULL, "dateModified" TIMESTAMP, "authorId" integer NOT NULL, "modifierId" integer, "isDeleted" boolean NOT NULL, "isActive" boolean NOT NULL, CONSTRAINT "PK_9309532197a7397548e341e5536" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "User" ("id" SERIAL NOT NULL, "dateCreated" TIMESTAMP NOT NULL, "dateModified" TIMESTAMP, "authorId" integer NOT NULL, "modifierId" integer, "isDeleted" boolean NOT NULL, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "isActive" boolean NOT NULL, "userType" integer NOT NULL, CONSTRAINT "PK_9862f679340fb2388436a5ab3e4" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "User"`, undefined);
        await queryRunner.query(`DROP TABLE "Role"`, undefined);
    }

}
