import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddingMessages1574602579305 implements MigrationInterface {
    name = 'AddingMessages1574602579305';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "Message" ("id" SERIAL NOT NULL, "dateCreated" TIMESTAMP NOT NULL, "dateModified" TIMESTAMP, "authorId" integer NOT NULL, "modifierId" integer, "isDeleted" boolean NOT NULL, "title" character varying NOT NULL, "content" character varying NOT NULL, "unread" boolean NOT NULL, "senderId" integer, "recipientId" integer, CONSTRAINT "PK_7dd6398f0d1dcaf73df342fa325" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "Message" ADD CONSTRAINT "FK_2bb6aae64e0ff3bdf45d6deabfe" FOREIGN KEY ("senderId") REFERENCES "User"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "Message" ADD CONSTRAINT "FK_24faf0aa712d246b798afbd8a63" FOREIGN KEY ("recipientId") REFERENCES "User"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Message" DROP CONSTRAINT "FK_24faf0aa712d246b798afbd8a63"`, undefined);
        await queryRunner.query(`ALTER TABLE "Message" DROP CONSTRAINT "FK_2bb6aae64e0ff3bdf45d6deabfe"`, undefined);
        await queryRunner.query(`DROP TABLE "Message"`, undefined);
    }

}
