import {MigrationInterface, QueryRunner} from 'typeorm';
import { RoleEntity } from '../model/entity/role.entity';

export class SeedRoles1573251312515 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      const rolesRepository = queryRunner.connection.getRepository(RoleEntity);
      const now = new Date();
      const adminRole = new RoleEntity();
      adminRole.dateCreated = now;
      adminRole.isDeleted = false;
      adminRole.isActive = true;
      adminRole.name = 'Admin';
      const userRole = new RoleEntity();
      userRole.dateCreated = now;
      userRole.isDeleted = false;
      userRole.isActive = true;
      userRole.name = 'User';
      await rolesRepository.save([
        adminRole,
        userRole,
      ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      //
    }

}
