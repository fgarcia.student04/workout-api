import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddingPassword1572813438564 implements MigrationInterface {
    name = 'AddingPassword1572813438564';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "User" ADD "password" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "User" DROP COLUMN "email"`, undefined);
        await queryRunner.query(`ALTER TABLE "User" ADD "email" character varying(254) NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "User" ADD CONSTRAINT "UQ_4a257d2c9837248d70640b3e36e" UNIQUE ("email")`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_4a257d2c9837248d70640b3e36" ON "User" ("email") `, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_4a257d2c9837248d70640b3e36"`, undefined);
        await queryRunner.query(`ALTER TABLE "User" DROP CONSTRAINT "UQ_4a257d2c9837248d70640b3e36e"`, undefined);
        await queryRunner.query(`ALTER TABLE "User" DROP COLUMN "email"`, undefined);
        await queryRunner.query(`ALTER TABLE "User" ADD "email" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "User" DROP COLUMN "password"`, undefined);
    }

}
