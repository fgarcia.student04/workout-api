import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnectionOptions, ConnectionOptions } from 'typeorm';

@Module({
  imports: [TypeOrmModule.forRootAsync({
    useFactory: async () => {
      const connectionOptions = await getConnectionOptions();
      const updatedConnectionOptions: ConnectionOptions = {
        ...connectionOptions,
        entities: [
          __dirname + '/../model/entity/*{.ts,.js}',
        ],
        migrations: [
          __dirname + '/../migrations/*{.ts,.js}',
        ],
      };
      return updatedConnectionOptions;
    },
  })],
})
export class DatabaseModule {}
