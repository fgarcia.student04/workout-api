import { Module } from '@nestjs/common';
import { ManagerModule } from './manager.module';
import { controllers } from '../controller';

@Module({
  imports: [ManagerModule],
  controllers: [...controllers],
})
export class ControllerModule {}
