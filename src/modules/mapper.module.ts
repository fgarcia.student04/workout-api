import { Module } from '@nestjs/common';
import { Mapper } from '../model/mapping/mapper';
import { profiles } from '../model/mapping';

@Module({
  providers: [Mapper, ...profiles],
  exports: [Mapper],
})
export class MapperModule {}
