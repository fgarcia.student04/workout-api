import { Module } from '@nestjs/common';
import { repositories } from '../repository';

@Module({
  providers: [...repositories],
  exports: [...repositories],
})
export class RepositoryModule {}
