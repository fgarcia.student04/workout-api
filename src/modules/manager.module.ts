import { Module } from '@nestjs/common';
import { RepositoryModule } from './repository.module';
import { MapperModule } from './mapper.module';
import { managers } from '../manager';
import { SocketGateway } from '../socket/gateway.socket';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from '../passportjs/local.strategy';
import { JwtStrategy } from '../passportjs/jwt.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '24h' },
    }),
    MapperModule,
    RepositoryModule,
  ],
  providers: [
    ...managers,
    LocalStrategy,
    JwtStrategy,
    SocketGateway,
  ],
  exports: [...managers],
})
export class ManagerModule {}
