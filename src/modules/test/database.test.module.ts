import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnectionOptions, ConnectionOptions, createConnection } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import * as dotenv from 'dotenv';

dotenv.config();

@Module({
  imports: [TypeOrmModule.forRootAsync({
    useFactory: async () => {
      const connectionOptions = await getConnectionOptions();
      const updatedConnectionOptions: ConnectionOptions = {
        ...connectionOptions,
        dropSchema: true,
        entities: [
          __dirname + '/../../model/entity/*{.ts,.js}',
        ],
        migrations: [
          __dirname + '/../../migrations/*{.ts,.js}',
        ],
      };

      const testSettings: Partial<PostgresConnectionOptions> = {
        port: Number(process.env.TYPEORM_TEST_PORT),
      };

      Object.assign(updatedConnectionOptions, testSettings);
      return updatedConnectionOptions;
    },
  })],
})
export class TestDatabaseModule {}
