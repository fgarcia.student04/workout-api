import { Controller, Get, Delete, HttpCode, HttpStatus } from '@nestjs/common';
import { UserManager } from '../../manager/Implementation/user.manager';
import { Id } from '../../utilities/decorator/bindId.parameter.decorator';
import { Roles } from '../../utilities/decorator/roles.method.decorator';
import { RoleEnum } from '../../model/enum/role.enum';
import { Authorize } from '../../utilities/decorator/authorize.class.decorator';

@Controller('users')
@Authorize()
export class UserController {
  constructor(
    private readonly userManager: UserManager,
  ) {}

  @Get()
  @Roles(RoleEnum.Admin)
  public async getAllUsers() {
    return await this.userManager.getAllUsers();
  }

  @Get(':id')
  @Roles(RoleEnum.Admin)
  public async getUserById(@Id() id: number) {
    return await this.userManager.getUserById(id);
  }

  @Delete(':id')
  @Roles(RoleEnum.Admin)
  @HttpCode(HttpStatus.NO_CONTENT)
  public async deleteUserById(@Id() id: number) {
    return await this.userManager.deleteUserById(id);
  }
}
