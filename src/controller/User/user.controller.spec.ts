import { UserController } from './user.controller';
import { UserManager } from '../../manager/Implementation/user.manager';
import { AdminUserSubmissionVm } from '../../model/vm/adminUserSubmission.vm';
import { UserTypeEnum } from '../../model/enum/userType.enum';
import { EntityNotFoundException } from '../../utilities/exception/entityNotFound.exception';
import { INestApplication } from '@nestjs/common';
import { createTestingApplication } from '../../../test/tools';

describe('UserController', () => {
  let app: INestApplication;
  let userController: UserController;
  let userManager: UserManager;

  beforeEach(async () => {
    app = await createTestingApplication();
    userController = app.get(UserController);
    userManager = app.get(UserManager);
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
  });

  function buildTestUserSubmissionVm(customize?: {
    clientType: number,
  }) {
    if (customize) {
      const customized = new AdminUserSubmissionVm();
      customized.firstName = `Test${Math.random()}`;
      customized.lastName = `User${Math.random()}`;
      customized.email = `test${Math.random()}@user.com`;
      customized.userType = customize.clientType;
      customized.password = 'secret';
      customized.roles = [1, 2];
      return customized;
    }
    const user = new AdminUserSubmissionVm();
    user.firstName = 'Test';
    user.lastName = 'User';
    user.email = `test${Math.random()}@user.com`;
    user.userType = UserTypeEnum.Client;
    user.password = 'secret';
    user.roles = [1, 2];
    return user;
  }

  describe('GetAllUsers', () => {
    it('should not be undefined', async () => {
      const users = await userController.getAllUsers();
      expect(users).not.toBeUndefined();
    });

    it('should return an array of users', async () => {
      const users = await userController.getAllUsers();
      expect(Array.isArray(users)).toBe(true);
    });
  });

  describe('GetUserById', () => {
    it('should get correct user from db by id', async () => {
      const testUser1 = buildTestUserSubmissionVm({
        clientType: UserTypeEnum.Client,
      });
      const testUser2 = buildTestUserSubmissionVm({
        clientType: UserTypeEnum.Trainer,
      });
      const createdUser1 = await userManager.createUserAdmin(testUser1, 1);
      const createdUser2 = await userManager.createUserAdmin(testUser2, 1);
      const retrievedUser1 = await userController.getUserById(createdUser1.id);
      const retrievedUser2 = await userController.getUserById(createdUser2.id);

      expect(createdUser1.id).toEqual(retrievedUser1.id);
      expect(createdUser1.email).toEqual(retrievedUser1.email);
      expect(createdUser1.name).toEqual(retrievedUser1.name);
      expect(createdUser1.userType).toEqual(retrievedUser1.userType);
      expect(createdUser1.userTypeDescription).toEqual(retrievedUser1.userTypeDescription);

      expect(createdUser2.id).toEqual(retrievedUser2.id);
      expect(createdUser2.email).toEqual(retrievedUser2.email);
      expect(createdUser2.name).toEqual(retrievedUser2.name);
      expect(createdUser2.userType).toEqual(retrievedUser2.userType);
      expect(createdUser2.userTypeDescription).toEqual(retrievedUser2.userTypeDescription);
    });

    it('should throw an exception when trying to retrieve a user that does not exist', async () => {
      await expect(userController.getUserById(0)).rejects.toThrowError(new EntityNotFoundException(0, 'User'));
    });
  });

  describe('DeleteUserById', () => {
    it('should delete user and return true', async () => {
      const testUser = buildTestUserSubmissionVm();
      const createdUser = await userManager.createUserAdmin(testUser, 1);
      const result = await userController.deleteUserById(createdUser.id);
      expect(result).toBeTruthy();
    });

    it('should throw an exception when trying to delete a user that does not exist', async () => {
      await expect(userController.deleteUserById(0)).rejects.toThrowError(new EntityNotFoundException(0, 'User'));
    });
  });
});
