import { Controller, Post, Request, UseGuards, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BindBody } from '../../utilities/decorator/bindBody.parameter.decorator';
import { UserSubmissionVm } from '../../model/vm/userSubmission.vm';
import { UserManager } from '../../manager/Implementation/user.manager';
import { AuthenticationManager } from '../../manager/Implementation/authentication.manager';
import { Authorize } from '../../utilities/decorator/authorize.class.decorator';
import { Roles } from '../../utilities/decorator/roles.method.decorator';
import { RoleEnum } from '../../model/enum/role.enum';
import { User } from '../../utilities/decorator/bindUser.parameter.decorator';
import { UserVm } from '../../model/vm/user.vm';
import { AdminUserSubmissionVm } from '../../model/vm/adminUserSubmission.vm';

@Controller('auth')
export class AuthenticationController {
  constructor(
    private readonly authManager: AuthenticationManager,
    private readonly userManager: UserManager,
  ) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  public async login(@Request() request) {
    return this.authManager.login(request.user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  public getProfile(@Request() request) {
    return request.user;
  }

  @Post('register')
  public async createUser(@BindBody() userSubmissionVm: UserSubmissionVm) {
    return await this.userManager.createUser(userSubmissionVm);
  }

  @Authorize()
  @Roles(RoleEnum.Admin)
  @Post('admin/register')
  public async createUserAdmin(
    @BindBody() userSubmissionVm: AdminUserSubmissionVm,
    @User() user: UserVm,
  ) {
    return await this.userManager.createUserAdmin(userSubmissionVm, user.id);
  }
}
