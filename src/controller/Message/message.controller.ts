import { Controller, Get } from '@nestjs/common';
import { Authorize } from '../../utilities/decorator/authorize.class.decorator';
import { MessageManager } from '../../manager/Implementation/message.manager';
import { Id } from '../../utilities/decorator/bindId.parameter.decorator';
import { User } from '../../utilities/decorator/bindUser.parameter.decorator';
import { UserVm } from '../../model/vm/user.vm';

@Controller('messages')
@Authorize()
export class MessageController {
  constructor(
    private readonly messageManager: MessageManager,
  ) {}

  @Get('conversation/:id')
  public async getConversationWithUserId(@Id() id: number, @User() user: UserVm) {
    return await this.messageManager.getConservasionBetweenUsers(id, user.id);
  }

  @Get()
  public async getCurrentlyUnreadMessages(@User() user: UserVm) {
    return await this.messageManager.getAllUnreadMessagesWithRecipientId(user.id);
  }
}
