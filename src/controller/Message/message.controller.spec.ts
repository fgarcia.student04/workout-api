import { INestApplication } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageManager } from '../../manager/Implementation/message.manager';
import { createTestingApplication } from '../../../test/tools';
import { UserManager } from '../../manager/Implementation/user.manager';
import { UserSubmissionVm } from '../../model/vm/userSubmission.vm';
import { UserTypeEnum } from '../../model/enum/userType.enum';
import { MessageSubmissionVm } from '../../model/vm/messageSubmission.vm';
import { UserVm } from '../../model/vm/user.vm';

describe('MessageController', () => {
  let app: INestApplication;
  let messageController: MessageController;
  let messageManager: MessageManager;

  let createdUser1: UserVm;
  let createdUser2: UserVm;

  beforeEach(async () => {
    app = await createTestingApplication();
    const userManager = app.get(UserManager);
    const user1 = new UserSubmissionVm();
    user1.email = 'user1@email.com';
    user1.firstName = 'user';
    user1.lastName = 'one';
    user1.password = 'secret';
    user1.userType = UserTypeEnum.Client;
    const user2 = new UserSubmissionVm();
    user1.email = 'user2@email.com';
    user1.firstName = 'user';
    user1.lastName = 'two';
    user1.password = 'secret';
    user1.userType = UserTypeEnum.Client;
    createdUser1 = await userManager.createUser(user1);
    createdUser2 = await userManager.createUser(user2);

    messageController = app.get(MessageController);
    messageManager = app.get(MessageManager);

    const newMessage = new MessageSubmissionVm();
    newMessage.content = 'Hey this is a test message';
    newMessage.senderId = createdUser1.id;
    newMessage.recipientId = createdUser2.id;
    await messageManager.createMessage(newMessage, createdUser1.id);

    const newMessage2 = new MessageSubmissionVm();
    newMessage2.content = 'Hey this is a test response';
    newMessage2.senderId = createdUser2.id;
    newMessage2.recipientId = createdUser1.id;
    const message2 = await messageManager.createMessage(newMessage2, createdUser2.id);
    await messageManager.updateMessagesToUnread([message2.id], createdUser2.id);
  });

  afterEach(async () => {
    if (app) {
      await app.close();
    }
  });

  describe('GetConversation', () => {
    it('should return an array of two messages', async () => {
      const messages = await messageController.getConversationWithUserId(createdUser2.id, createdUser1);
      expect(Array.isArray(messages)).toEqual(true);
      expect(messages.length).toEqual(2);
    });
  });

  describe('GetUnreadMessages', () => {
    it('should return an array of one unread message', async () => {
      const messages = await messageController.getCurrentlyUnreadMessages(createdUser1);
      expect(Array.isArray(messages)).toEqual(true);
      expect(messages.length).toEqual(1);
      const unreadMessage = messages[0];
      expect(unreadMessage.unread).toBe(true);
    });
  });
});
