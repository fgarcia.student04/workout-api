import { UserController } from './User/user.controller';
import { AuthenticationController } from './Authentication/authentication.controller';
import { MessageController } from './Message/message.controller';

export const controllers = [
  UserController,
  AuthenticationController,
  MessageController,
];
