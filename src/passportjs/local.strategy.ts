import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthenticationManager } from '../manager/Implementation/authentication.manager';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authManager: AuthenticationManager) {
    super({
      usernameField: 'email',
    });
  }

  async validate(email: string, password: string): Promise<any> {
    const user = await this.authManager.verifyUser(email, password);
    if (user) {
      return user;
    }
    throw new UnauthorizedException();
  }
}
