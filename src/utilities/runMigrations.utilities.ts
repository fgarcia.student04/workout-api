import { INestApplication } from '@nestjs/common';
import { Connection } from 'typeorm';
export async function runMigrations(app: INestApplication) {
  const connection = app.get(Connection);
  await connection.runMigrations({ transaction: 'each' });
}
