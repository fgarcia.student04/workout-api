export class EntityNotFoundException extends Error {
  constructor(id: any, entityType: string) {
    super(`No ${entityType} found with id: ${id}`);
  }
}
