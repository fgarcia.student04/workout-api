export class MissingTypeException extends Error {
  constructor(route: string) {
    super(`No class provided to body for route ${route}`);
  }
}
