export class NoBodyException extends Error {
  constructor(route: string) {
    super(`No body provided for route ${route}`);
  }
}
