export class DuplicateValueException extends Error {
  constructor() {
    super('No duplicate property values allowed');
  }
}
