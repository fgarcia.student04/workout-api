import { Body, ValidationPipe } from '@nestjs/common';

export const BindBody = (property?: string) => {
  if (property) {
    return Body(property, new ValidationPipe({ transform: true }));
  }
  return Body(new ValidationPipe({ transform: true }));
};
