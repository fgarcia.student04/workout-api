import { Param, ValidationPipe } from '@nestjs/common';

export const Id = () => Param('id', new ValidationPipe({ transform: true }));
