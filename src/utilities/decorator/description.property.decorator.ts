import 'reflect-metadata';
import { DescriptionKey, DescriptionAccessor } from '../constant.utilities';

export const Description = (description: string) => {
  return (target: object, key: string) => {
    Reflect.defineMetadata(DescriptionKey, description, target, key);
    Reflect.defineMetadata(DescriptionAccessor, key, target, target[key]);
  };
};

export const GetDescription = (target: object, value: any) => {
  const property = Reflect.getMetadata(DescriptionAccessor, target, value);
  return Reflect.getMetadata(DescriptionKey, target, property);
};
