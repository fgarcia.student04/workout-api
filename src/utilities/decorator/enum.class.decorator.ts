import { DuplicateValueException } from '../exception/duplicateValue.exception';

export const Enum = (target: object) => {
  // do not allow duplicates
  const anyDuplicate = {};
  Object.keys(target).forEach((key) => {
    const value = target[key];
    if (anyDuplicate[value]) {
      throw new DuplicateValueException();
    }
    anyDuplicate[value] = true;
  });
};
