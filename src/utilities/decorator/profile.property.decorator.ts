import 'reflect-metadata';
import { Mapper } from '../../model/mapping/mapper';
import { ProfileKey } from '../constant.utilities';

export const Profile = <T, V>(
  SourceClass: new() => T,
  DestinationClass: new() => V,
) => {
  return (target: object, key: string) => {
    const newTarget = target.constructor as new() => object;
    const value = new newTarget()[key];
    // add the profile
    Reflect.defineMetadata(ProfileKey, value, Mapper, `${SourceClass.name}->${DestinationClass.name}`);
  };
};
