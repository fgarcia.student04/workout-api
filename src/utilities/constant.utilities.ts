export const DescriptionKey = Symbol('description');
export const DescriptionAccessor = Symbol('description accessor');
export const ProfileKey = Symbol('profile');
export const EventTypes = {
  Message: 'message',
  Info: 'info',
  Error: 'error',
};
