import { Description } from '../../utilities/decorator/description.property.decorator';
import { Enum } from '../../utilities/decorator/enum.class.decorator';

@Enum
export class RoleEnum {
  @Description('User')
  public static User = 1;
  @Description('Admin')
  public static Admin = 2;
}
