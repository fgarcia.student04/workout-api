import { Description } from '../../utilities/decorator/description.property.decorator';
import { Enum } from '../../utilities/decorator/enum.class.decorator';

@Enum
export class UserTypeEnum {
  @Description('Client')
  public static Client = 1;
  @Description('Trainer')
  public static Trainer = 2;
}
