export class UserDto {
  public id: number = 0;
  public firstName: string = '';
  public lastName: string = '';
  public email: string = '';
  public password: string = '';
  public userType: number = 0;
}
