import { BaseEntity } from './base.entity';
import { Entity, Column, ManyToMany } from 'typeorm';
import { UserEntity } from './user.entity';

@Entity({ name: 'Role' })
export class RoleEntity extends BaseEntity {
  @Column()
  public isActive: boolean = false;

  @Column()
  public name: string = '';

  @ManyToMany(() => UserEntity, user => user.roles)
  public users: UserEntity[];
}
