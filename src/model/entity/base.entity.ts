import {PrimaryGeneratedColumn, Column} from 'typeorm';

export class BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number = 0;

  @Column({ type: 'timestamp' })
  public dateCreated: Date = new Date();

  @Column({ type: 'timestamp', nullable: true })
  public dateModified?: Date = null;

  @Column()
  public authorId: number = 0;

  @Column({ nullable: true })
  public modifierId?: number = null;

  @Column()
  public isDeleted: boolean = false;
}
