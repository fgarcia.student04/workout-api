import { BaseEntity } from './base.entity';
import { Entity, Column, ManyToOne } from 'typeorm';
import { UserEntity } from './user.entity';

@Entity({ name: 'Message' })
export class MessageEntity extends BaseEntity {
  @Column()
  public title: string = '';

  @Column()
  public content: string = '';

  @Column()
  public unread: boolean = false;

  @Column({ nullable: true })
  public senderId: number | null = 0;

  @Column({ nullable: true })
  public recipientId: number | null = 0;

  @ManyToOne(() => UserEntity)
  public sender: UserEntity;

  @ManyToOne(() => UserEntity)
  public recipient: UserEntity;
}
