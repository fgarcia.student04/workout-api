import {Entity, Column, ManyToMany, JoinTable, Index} from 'typeorm';
import { BaseEntity } from './base.entity';
import { RoleEntity } from './role.entity';

@Entity({ name: 'User' })
export class UserEntity extends BaseEntity {
  @Column()
  public firstName: string = '';

  @Column()
  public lastName: string = '';

  @Column({ unique: true, length: 254 })
  @Index()
  public email: string = '';

  @Column()
  public password: string = '';

  @Column()
  public isActive: boolean = false;

  @Column()
  public userType: number = 0;

  @ManyToMany(() => RoleEntity, role => role.users)
  @JoinTable()
  public roles: RoleEntity[];
}
