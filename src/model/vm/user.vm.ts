import { RoleVm } from './role.vm';

export class UserVm {
  public id: number = 0;
  public name: string = '';
  public email: string = '';
  public userType: number = 0;
  public userTypeDescription: string = '';
  public roles: RoleVm[] = [];
}
