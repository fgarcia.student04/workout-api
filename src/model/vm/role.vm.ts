export class RoleVm {
  public id: number = 0;
  public isActive: boolean = false;
  public name: string = '';
}
