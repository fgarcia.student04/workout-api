import { MessageUserVm } from './messageUser.vm';
export class MessageVm {
  public id: number = 0;

  public title: string = '';

  public content: string = '';

  public unread: boolean = false;

  public sender: MessageUserVm;

  public recipient: MessageUserVm;
}
