import { IsNotEmpty, IsString, IsEmail, IsEnum, IsPositive, IsNotIn } from 'class-validator';

export class MessageSubmissionVm {
  @IsString()
  public title: string = '';

  @IsString()
  @IsNotEmpty()
  public content: string = '';

  @IsPositive()
  @IsNotEmpty()
  @IsNotIn([0])
  public senderId: number = 0;

  @IsPositive()
  @IsNotEmpty()
  @IsNotIn([0])
  public recipientId: number = 0;
}
