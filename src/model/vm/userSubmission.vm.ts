import { IsNotEmpty, IsString, IsEmail, IsEnum } from 'class-validator';
import { UserTypeEnum } from '../enum/userType.enum';

export class UserSubmissionVm {
  @IsString()
  @IsNotEmpty()
  public firstName: string = '';

  @IsString()
  @IsNotEmpty()
  public lastName: string = '';

  @IsEmail()
  @IsNotEmpty()
  public email: string = '';

  @IsString()
  @IsNotEmpty()
  public password: string = '';

  @IsEnum(UserTypeEnum)
  public userType: number = 0;
}
