import { IProfile } from './mapper';
import { UserEntity } from '../entity/user.entity';
import { UserVm } from '../vm/user.vm';
import { GetDescription } from '../../utilities/decorator/description.property.decorator';
import { UserTypeEnum } from '../enum/userType.enum';
import { Injectable } from '@nestjs/common';
import { Profile } from '../../utilities/decorator/profile.property.decorator';
import { UserSubmissionVm } from '../vm/userSubmission.vm';
import * as bcrypt from 'bcrypt';
import { ignoreBaseFields } from './util';
import { AdminUserSubmissionVm } from '../vm/adminUserSubmission.vm';

@Injectable()
export class UserMapping {
  private static saltRounds = 10;

  @Profile(UserEntity, UserVm)
  public EntityToVm: IProfile<UserEntity, UserVm> = {
    name: (config) => config.mapFrom(src => `${src.firstName} ${src.lastName}`),
    userTypeDescription: (config) => config.mapFrom(src => GetDescription(UserTypeEnum, src.userType)),
    roles: (config) => config.mapFrom(src => undefined),
  };

  @Profile(UserSubmissionVm, UserEntity)
  public VmToEntity: IProfile<UserSubmissionVm, UserEntity> = ignoreBaseFields({
    isActive: (config) => config.ignore(),
    roles: (config) => config.ignore(),
    password: (config) => config.mapFrom((vm) => bcrypt.hashSync(vm.password, UserMapping.saltRounds)),
  });

  @Profile(AdminUserSubmissionVm, UserEntity)
  public AdminVmToEntity: IProfile<AdminUserSubmissionVm, UserEntity> = ignoreBaseFields({
    isActive: (config) => config.ignore(),
    roles: (config) => config.ignore(),
    password: (config) => config.mapFrom((vm) => bcrypt.hashSync(vm.password, UserMapping.saltRounds)),
  });
}
