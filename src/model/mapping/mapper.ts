// tslint:disable:no-var-requires
import 'reflect-metadata';
import { ProfileKey } from '../../utilities/constant.utilities';

let Injectable;
try {
  Injectable = require('@nestjs/common').Injectable;
} catch (e) {
  Injectable = () => () => void(0);
}

const MapperIgnore = Symbol('Mapper_Ignore');

type MapperFunction<V> = (obj: V) => any;

type Constructor<T> = new (...args: any[]) => T;

export interface MapObjectsConfig<V> {
  mapFrom: (mapFromSource: MapperFunction<V>) => any;
  ignore: () => symbol;
}

type MapObjectsPropertyConfigFunction<V> = (config: MapObjectsConfig<V>) => any | symbol;

type Profile<T, V> = {
  [P in keyof V]?: MapObjectsPropertyConfigFunction<T>;
};

@Injectable()
export class Mapper {
  public Map<T, V>(
    source: T[],
    target: Constructor<V>,
  ): V[];
  public Map<T, V>(
    source: T,
    target: Constructor<V>,
  ): V;
  public Map<T, V>(
    source: T | T[],
    target: Constructor<V>,
  ): V | V[] {
    if (Array.isArray(source)) {
      return source.map((src) => Mapper.InternalMapObject(src, target));
    }
    return Mapper.InternalMapObject(source, target);
  }

  private static InternalMapObject<T, V>(
    source: T,
    target: Constructor<V>,
  ): V {
    const mapperOptions = Reflect.getMetadata(ProfileKey, Mapper, `${source.constructor.name}->${target.name}`);
    const destination = new target();
    const destinationKeys = Object.keys(destination);
    const alreadyMapped = {};
    if (mapperOptions) {
      const config = Mapper.BuildConfig(source);
      const optionKeys = Object.keys(mapperOptions);
      optionKeys.forEach((optionKey) => {
        const passedUserConfigValue = mapperOptions[optionKey as keyof V](config);
        if (passedUserConfigValue !== MapperIgnore) {
          destination[optionKey] = passedUserConfigValue;
        }
        alreadyMapped[optionKey] = true;
      });
    }
    destinationKeys.forEach((key) => {
      if (!alreadyMapped[key]) {
        destination[key] = source[key];
      }
    });
    return destination;
  }

  private static BuildConfig<T>(source: T) {
    return {
      mapFrom: (mapperFn: MapperFunction<T>) => mapperFn(source),
      ignore: () => MapperIgnore,
    };
  }
}

export type IProfile<T, V> = Profile<T, V>;
