import { UserMapping } from './user.mapping';
import { MessageProfile } from './message.mapping';
export const profiles = [
  UserMapping,
  MessageProfile,
];
