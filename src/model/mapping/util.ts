import { IProfile } from './mapper';

export const ignoreBaseFields = <T, V>(obj: IProfile<T, V>): IProfile<T, V> => ({
  ...obj,
  id: (config) => config.ignore(),
  dateCreated: (config) => config.ignore(),
  isDeleted: (config) => config.ignore(),
  dateModified: (config) => config.ignore(),
  authorId: (config) => config.ignore(),
  modifierId: (config) => config.ignore(),
});
