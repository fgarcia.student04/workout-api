import { Injectable } from '@nestjs/common';
import { Profile } from '../../utilities/decorator/profile.property.decorator';
import { MessageSubmissionVm } from '../vm/messageSubmission.vm';
import { MessageEntity } from '../entity/message.entity';
import { IProfile } from './mapper';
import { ignoreBaseFields } from './util';

@Injectable()
export class MessageProfile {
  @Profile(MessageSubmissionVm, MessageEntity)
  public VmToEntity: IProfile<MessageSubmissionVm, MessageEntity> = ignoreBaseFields({});
}
