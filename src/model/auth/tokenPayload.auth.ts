export class TokenPayload {
  public userId: number;
  public email: string;
}
